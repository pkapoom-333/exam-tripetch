import React, { useState } from "react";
import { Grid, Box, Typography, MobileStepper } from "@mui/material";
import { styled } from "@mui/material/styles";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import FootballerImage from "../../assets/image/footballer 1.svg";
import Players from "../../assets/image/Group 3.svg";

const CustomMobileStepper = styled(MobileStepper)(({ theme }) => ({
  "& .MuiMobileStepper-dot": {
    width: 10,
    height: 10,
    margin: "0 10px",
  },
}));

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const RenderTitle = ({ isLeft, title }) => {
  return (
    <Grid
      item
      lg={6}
      md={8}
      sm={8}
      xs={12}
      marginTop={{ lg: "40px", md: "40px", sm: "40px", sx: "10px" }}
    >
      <Box
        display="flex"
        alignItems="center"
        height={{ lg: "105px", md: "105px", sm: "80px", xs: "80px" }}
        paddingLeft={{
          lg: isLeft ? "25%" : "10%",
          md: "10%",
          sm: "10%",
          xs: "3%",
        }}
        paddingRight={{
          lg: isLeft ? "10%" : "15%",
          md: "10%",
          sm: isLeft ? "13%" : "10%",
          xs: "3%",
        }}
      >
        <Typography variant="h2" color="#E7E7E7">
          {title}
        </Typography>
      </Box>
    </Grid>
  );
};

const RenderImage = ({ src, title, isLeft }) => {
  const style = {
    display: { sm: "flex" },
    alignItems: { sm: "center" },
    justifyContent: { sm: "center" },
    position: {
      lg: "absolute",
      md: "absolute",
      sm: "absolute",
      sx: "relative",
    },
    left: {
      lg: !isLeft ? "0" : "",
      md: !isLeft ? "0" : "",
      sm: !isLeft ? "0" : "",
    },
    right: {
      lg: isLeft ? "0" : "",
      md: isLeft ? "0" : "",
      sm: isLeft ? "0" : "",
    },
    transform: {
      lg: !isLeft
        ? "translateX(10%) translateY(0%)"
        : "translateX(-10%)  translateY(-15%)",
      md: !isLeft
        ? "translateX(-30%) translateY(-10%)"
        : "translateX(0%)  translateY(-10%)",
      sm: !isLeft
        ? "translateX(-30%) translateY(5%)"
        : "translateX(0%)  translateY(-10%)",
      xs: !isLeft
        ? "translateX(15%) translateY(0%)"
        : "translateX(0%)  translateY(20%)",
    },
    maxWidth: {
      lg: "100%",
      md: "60%",
      sm: "60%",
      xs: !isLeft ? "80%" : "100%",
    },
    "@media (max-width: 1200px) and (min-width: 1001px)": isLeft
      ? {
          width: "50%",
          height: "50%",
          objectFit: "cover",
          objectPosition: "0% 40%",
        }
      : {
          width: "80%",
          height: "80%",
        },
    "@media (max-width: 1000px) and (min-width: 801px)": isLeft
      ? {
          width: "50%",
          height: "50%",
          objectFit: "cover",
          objectPosition: "0% 40%",
        }
      : {},
    "@media (max-width: 800px) and (min-width: 600px)": isLeft
      ? {
          height: "65%",
          width: "65%",
          objectFit: "cover",
          objectPosition: "0% 50%",
        }
      : {},
    "@media (max-width: 599px) and (min-width: 340px)": {
      height: "100%",
      width: "100%",
    },
  };

  return (
    <Grid
      item
      lg={6}
      md={4}
      sm={4}
      xs={12}
      marginTop={{ lg: "40px", md: "40px", sm: "40px", sx: "10px" }}
      height={{ lg: "auto", md: "auto", sm: "auto", xs: "295px" }}
    >
      <Box component="img" src={src} alt={title} sx={style} />
    </Grid>
  );
};

const RenderContent = ({
  title,
  dataIndex,
  isLeft,
  background,
  caption,
  number,
  getColorOfLine,
  detail,
  getColorOfSubtitle,
}) => {
  const numberOfData = dataIndex + 1;
  return (
    <Grid
      key={dataIndex}
      item
      xs={12}
      sx={{
        background: background,
        display: {
          lg: "block",
          md: "block",
          sm: "block",
          xs: "none",
        },
      }}
    >
      <Grid container justifyContent={isLeft ? "flex-start" : "flex-end"}>
        <Grid item xs={false} lg={6} md={8} sm={8}>
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            minHeight={{
              lg: "260px",
              md: "200px",
              sm: "200px",
            }}
            paddingLeft={{
              lg: isLeft ? "25%" : "10%",
              md: "10%",
              sm: "10%",
              xs: "3%",
            }}
            paddingRight={{
              lg: isLeft ? "25%" : "15%",
              md: "10%",
              sm: isLeft ? "13%" : "10%",
              xs: "3%",
            }}
          >
            <Box display="flex" alignItems="center">
              <Box display="flex" flexDirection="column">
                <Typography
                  variant="caption"
                  color={caption ? caption : "#000000"}
                >
                  {number}
                </Typography>
                <Box
                  name="line"
                  sx={{
                    width: "100%",
                    height: 5,
                    borderRadius: "5px",
                    background: getColorOfLine(numberOfData),
                  }}
                />
              </Box>
              <Typography
                variant="h4"
                color={"#C2C2C2"}
                sx={{
                  marginLeft: "10px",
                  maxWidth: "95%",
                  overflowX: "hidden",
                }}
              >
                {title}
              </Typography>
            </Box>
            <Typography
              variant="subtitle1"
              color={getColorOfSubtitle(numberOfData)}
              sx={{
                marginTop: "10px",
              }}
            >
              {detail}
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Grid>
  );
};

const RenderAutoPlaySwipeableViews = ({
  background,
  activeStep,
  handleStepChange,
  listdata,
  lengthData,
  getColorOfLine,
  getColorOfSubtitle,
}) => {
  return (
    <Grid
      item
      lg={false}
      md={false}
      sm={false}
      xs={12}
      sx={{
        background: background,
        display: { lg: "none", md: "none", sm: "none", sx: "block" },
      }}
    >
      <AutoPlaySwipeableViews
        axis={"x"}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {listdata.map((data, dataIndex) => (
          <Box
            key={data.title}
            display="flex"
            flexDirection="column"
            justifyContent="center"
            minHeight="300px"
            paddingTop={"40px"}
            paddingLeft={"3%"}
            paddingRight={"3%"}
          >
            <Box display="flex" alignItems="center">
              <Box display="flex" flexDirection="column">
                <Typography
                  variant="caption"
                  color={data.caption ? data.caption : "#000000"}
                >
                  {data.number}
                </Typography>
                <Box
                  name="line"
                  sx={{
                    width: "100%",
                    height: 5,
                    borderRadius: "5px",
                    background: getColorOfLine(dataIndex + 1),
                  }}
                />
              </Box>
              <Typography
                variant="h4"
                color={"#C2C2C2"}
                sx={{
                  marginLeft: "10px",
                  maxWidth: "95%",
                  overflowX: "hidden",
                }}
              >
                {data.title}
              </Typography>
            </Box>
            <Typography
              variant="subtitle1"
              color={getColorOfSubtitle(dataIndex + 1)}
              sx={{
                marginTop: "10px",
              }}
            >
              {data.detail}
            </Typography>
          </Box>
        ))}
      </AutoPlaySwipeableViews>
      <Box
        display={"flex"}
        alignItems={"center"}
        justifyContent={"center"}
        sx={{
          width: "100%",
        }}
      >
        <CustomMobileStepper
          steps={lengthData}
          position="static"
          activeStep={activeStep}
          sx={{
            height: "30px",
            width: "100%",
            justifyContent: "center",
            background: "transparent",
          }}
        />
      </Box>
    </Grid>
  );
};

export const HomePage = () => {
  const sections = [
    {
      title: "ATHLETS",
      data: [
        {
          number: "01",
          title: "CONNECTION",
          detail:
            "Connect with coaches directly, you can ping coaches to view profile.",
          background: "#FFFFFF",
        },
        {
          number: "02",
          title: "COLLABORATION",
          detail:
            "Work with other student athletes to increase visibility. When you share and like other players' videos, it will increase your visibility as a player. This is the teamwork aspect of Surface 1.",
          background: "#F5F4F9",
        },
        {
          number: "03",
          title: "GROWTH",
          detail:
            "Resources and tools for you to get better as a student athlete. Access to training classes, tutor sessions, etc.",
          background: "#5E3DB3",
        },
      ],
      src: FootballerImage,
    },
    {
      title: "PLAYERS",
      data: [
        {
          number: "01",
          title: "CONNECTION",
          detail:
            "Connect with talented athletes directly, you can watch their skills through video showreels directly from Surface 1.",
          background: "#FFFFFF",
        },
        {
          number: "02",
          title: "COLLABORATION",
          detail:
            "Work with recruiters to increase your chances of finding talented athletes.",
          background: "#F5F4F9",
        },
        {
          number: "03",
          title: "GROWTH",
          detail: "Save your time, recruit proper athletes for your team.",
          background: "#090C35",
          caption: "#FFFFFF",
        },
      ],
      src: Players,
    },
  ];
  const [activeStep, setActiveStep] = useState(0);

  const getColorOfLine = (number) => {
    if (number % 3 === 0) {
      return "#FFFFFF";
    }

    return "#603EBE";
  };

  const getColorOfSubtitle = (number) => {
    if (number % 3 === 0) {
      return "#FFFFFF";
    }

    return "#000000";
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Box sx={{ flexGrow: 1, maxWidth: "100%", overflowX: "hidden" }}>
      {sections.map((section, sectionIndex) => {
        const isLeft = (sectionIndex + 1) % 2 === 0;
        const isAmarginTop = sectionIndex === 0;

        return (
          <Grid
            container
            key={sectionIndex}
            sx={{
              marginTop: {
                lg: isAmarginTop ? "0px" : "100px",
                md: isAmarginTop ? "0px" : "100px",
              },
              overflowX: "hidden",
            }}
            direction={isLeft ? "row" : "row-reverse"}
            justifyContent={isLeft ? "flex-start" : "flex-end"}
          >
            {/* Title */}
            <RenderTitle title={section.title} isLeft={isLeft} />
            {/* Image */}
            <RenderImage
              src={section.src}
              title={section.title}
              isLeft={isLeft}
            />
            {/* Content */}
            {section.data.map((data, dataIndex) => {
              return (
                <RenderContent
                  key={dataIndex}
                  dataIndex={dataIndex}
                  isLeft={isLeft}
                  caption={data.caption}
                  number={data.number}
                  background={data.background}
                  title={data.title}
                  detail={data.detail}
                  getColorOfSubtitle={getColorOfSubtitle}
                  getColorOfLine={getColorOfLine}
                />
              );
            })}
            {/* AutoPlaySwipeableViews */}
            <RenderAutoPlaySwipeableViews
              activeStep={activeStep}
              background={section.data[activeStep].background}
              listdata={section.data}
              lengthData={section.data.length}
              handleStepChange={handleStepChange}
              getColorOfLine={getColorOfLine}
              getColorOfSubtitle={getColorOfSubtitle}
            />
          </Grid>
        );
      })}
    </Box>
  );
};
